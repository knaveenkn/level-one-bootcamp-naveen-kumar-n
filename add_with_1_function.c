//Write a program to add two user input numbers using one function.
#include<stdio.h>
struct fract
{
    int  num;
    int  deno;
};
typedef struct fract fraction;
fraction input()
{
    fraction a;
    printf("Enter the  numerator:");
    scanf("%d",&a.num);
    printf("Enter the  denominator:");
    scanf("%d",&a.deno);
    return a;
}
fraction sum(fraction a,fraction b)
{
    fraction res;
    if(a.deno==b.deno)
    {
        res.deno=a.deno;
        res.num=a.num+b.num;
    }
    else
    {
        res.deno=a.deno*b.deno;
      res.num=(a.num*b.deno)+(b.num*a.deno);
    }
    return res;
}
int main()
{
    fraction a,b,c;
    a=input();
    b=input();
    c=sum(a,b);
    printf("The final sum is %d/%d which is %.2f ",c.num,c.deno,(c.num/(1.0*c.deno))*1);
    return 0;
}