//Write a program to add two user input numbers using 4 functions.#include <stdio.h>



#include <stdio.h>

int get_input()
{
    int a;
    printf("input the number:");
    scanf("%d",&a);
    return a;
}

int get_sum(int x,int y)
{
    int sum;
    sum = x+y;
    return sum;
}

void get_output(int x , int y,int z)
{
    printf("the sum of %d and %d is %d",x,y,z);
}

int main()
{
    int a,b,c;
    a = get_input();
    b = get_input();
    c = get_sum(a,b);
    get_output(a,b,c);
}


